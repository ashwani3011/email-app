import { Link } from "react-router-dom";
import { useSelector } from "react-redux";

//css
import "./EmailList.css";

export default function EmailList() {
  const { emails } = useSelector((store) => store.email);
  if (!emails) {
    return <h2>Loading...</h2>;
  }
  return (
    <div className="preview-email-container">
      {emails.map((d) => (
        <div key={d.id} className="email">
          <Link to={`/emails/${d.id}`}>
            <div className="preview-email">
              <span className="preview-email-subject">{d.subject}</span>
              <span className="preview-email-body"> - {d.body}</span>
            </div>
          </Link>
        </div>
      ))}
    </div>
  );
}
