import { useState } from "react";
import { useHistory } from "react-router-dom";
import { HiOutlineSearch } from "react-icons/hi";
import { IoMdOptions } from "react-icons/io";

// css
import "./Searchbar.css";

export default function Searchbar() {
  const [term, setTerm] = useState("");
  const history = useHistory();

  const handleSubmit = (e) => {
    e.preventDefault();
    let correctTerm = term.trim().toLocaleLowerCase();
    history.push(`/search?q=${correctTerm}`);
  };

  return (
    <div className="searchbar">
      <HiOutlineSearch className="searchbar-icon" />
      <form onSubmit={handleSubmit}>
        <input
          className="input"
          type="text"
          onChange={(e) => setTerm(e.target.value)}
          required
          placeholder="Search mail"
        />
      </form>
      <IoMdOptions className="searchbar-icon" />
    </div>
  );
}
