import { NavLink } from "react-router-dom";
import { setFilter, filterEmails } from "../features/email/emailSlice";
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";

//css
import "./Sidebar.css";

const filterList = [
  {
    icon: "https://img.icons8.com/material-sharp/28/000000/inbox.png",
    text: "inbox",
  },
  {
    icon: "https://img.icons8.com/plumpy/28/000000/task.png",
    text: "draft",
  },
  { icon: "https://img.icons8.com/plumpy/28/000000/spam.png", text: "spam" },
  { icon: "https://img.icons8.com/plumpy/28/000000/trash.png", text: "trash" },
  {
    icon: "https://img.icons8.com/material-two-tone/28/000000/new-post.png",
    text: "all",
  },
];

export default function Sidebar() {
  const dispatch = useDispatch();
  const { filter: f } = useSelector((store) => store.email);
  return (
    <aside className="sidebar-aside">
      {filterList.map((filter, i) => {
        return (
          <NavLink
            className={f === filter.text ? "active" : ""}
            key={i}
            to={`/${filter.text}`}
            onClick={(e) => {
              dispatch(setFilter(filter.text));
              dispatch(filterEmails(filter.text));
            }}
          >
            <div className="single-filter">
              <img src={filter.icon} alt="" />
              {filter.text[0].toLocaleUpperCase() + filter.text.slice(1)}
            </div>
          </NavLink>
        );
      })}
    </aside>
  );
}
