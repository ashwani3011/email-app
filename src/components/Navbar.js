import { BiMenu } from "react-icons/bi";
import { VscQuestion } from "react-icons/vsc";
import { MdOutlineSettings } from "react-icons/md";
import { CgMenuGridO } from "react-icons/cg";

//css
import "./Navbar.css";

//pages and components
import Searchbar from "./Searchbar";
export default function Navbar() {
  return (
    <div className="navbar">
      <div className="navbar-left">
        <BiMenu className="icon" />
        <img
          src="https://img.icons8.com/fluency/48/000000/gmail-new.png"
          alt="logo"
        />
        <p className="title navbar-title">Email</p>
      </div>

      <Searchbar />
      <div className="navbar-right">
        <VscQuestion className="icon" />
        <MdOutlineSettings className="icon" />
        <CgMenuGridO className="icon" />
        <div className="avatar">
          <img
            src="https://www.kindpng.com/picc/m/78-786207_user-avatar-png-user-avatar-icon-png-transparent.png"
            alt="avatar"
          />
        </div>
      </div>
    </div>
  );
}
