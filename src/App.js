import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import { useSelector } from "react-redux";
import { getAllEmails } from "./features/email/emailSlice";
import { useEffect } from "react";
import { useDispatch } from "react-redux";

//css
import "./App.css";

//pages and components

import Search from "./pages/search/Search";
import Email from "./pages/email/Email";
import Trash from "./pages/trash/Trash";
import Spam from "./pages/spam/Spam";
import Draft from "./pages/draft/Draft";
import Inbox from "./pages/inbox/Inbox";
import All from "./pages/all/All";
import Navbar from "./components/Navbar";
import Sidebar from "./components/Sidebar";

export default function App() {
  const { isLoading } = useSelector((store) => store.email);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getAllEmails());
  }, [dispatch]);

  return (
    <div className="app">
      <BrowserRouter>
        <nav>
          <Navbar />
        </nav>
        <main>
          <Sidebar className="sidebar" />
          {isLoading && <h2 className="loading-message">Loading...</h2>}
          {!isLoading && (
            <div className="dashboard">
              <Switch className="dashboard">
                <Route exact path="/">
                  <Inbox />
                </Route>
                <Route path="/search">
                  <Search />
                </Route>
                <Route path="/inbox">
                  <Inbox />
                </Route>
                <Route path="/draft">
                  <Draft />
                </Route>
                <Route path="/spam">
                  <Spam />
                </Route>
                <Route path="/trash">
                  <Trash />
                </Route>
                <Route path="/all">
                  <All />
                </Route>
                <Route path="/emails/:id">
                  <Email />
                </Route>
                <Route path="*">
                  <Redirect to="/" />
                </Route>
              </Switch>
            </div>
          )}
        </main>
      </BrowserRouter>
    </div>
  );
}
