import { useDispatch } from "react-redux";
import { useEffect } from "react";
import { setFilter, filterEmails } from "../../features/email/emailSlice";

//css
import "./Spam.css";

//components and pages
import EmailList from "../../components/EmailList";

export default function Spam() {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(setFilter("spam"));
    dispatch(filterEmails("spam"));
  }, [dispatch]);

  return (
    <div>
      <h2>Spam</h2>
      <div className="main-container">
        <main>
          <EmailList />
        </main>
      </div>
    </div>
  );
}
