import { useDispatch } from "react-redux";
import { useEffect } from "react";
import { setFilter, filterEmails } from "../../features/email/emailSlice";

//css
import "./Trash.css";

//components and pages
import EmailList from "../../components/EmailList";

export default function Trash() {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(setFilter("trash"));
    dispatch(filterEmails("trash"));
  }, [dispatch]);

  return (
    <div>
      <h2>Trash</h2>
      <div className="main-container">
        <main>
          <EmailList />
        </main>
      </div>
    </div>
  );
}
