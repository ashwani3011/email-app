import { useDispatch } from "react-redux";
import { useEffect } from "react";
import { setFilter, filterEmails } from "../../features/email/emailSlice";

//css
import "./Inbox.css";

//components and pages
import EmailList from "../../components/EmailList";

export default function Inbox() {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(setFilter("inbox"));
    dispatch(filterEmails("inbox"));
  }, [dispatch]);
  return (
    <div>
      <h2>Inbox</h2>
      <div className="main-container">
        <main>
          <EmailList />
        </main>
      </div>
    </div>
  );
}
