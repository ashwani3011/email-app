import { useParams } from "react-router";
import { useSelector } from "react-redux";
import { useDispatch } from "react-redux";
import { useEffect } from "react";
import { setFilter, filterEmails } from "../../features/email/emailSlice";

//css
import "./Email.css";

export default function Email() {
  const { emails, filter, isLoading } = useSelector((store) => store.email);
  const { id } = useParams();

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(setFilter(filter));
    dispatch(filterEmails("all"));
  }, [id, filter, dispatch]);

  console.log(emails, filter);
  let email = emails[id - 1];

  if (isLoading) {
    return <h2 className="loading-message">Loading...</h2>;
  }
  if (!email) {
    return <h4 className="loading-message">No such email found.</h4>;
  }
  if (email) {
    return (
      <div className="email-full-view">
        <div className="heading">
          <p className="title">{email.subject}</p>
          <span>{email.tag}</span>
        </div>
        <p className="body">{email.body}</p>
      </div>
    );
  }
}
