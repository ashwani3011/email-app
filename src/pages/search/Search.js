import { useLocation } from "react-router-dom";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";

// css
import "./Search.css";

export default function Search() {
  const queryString = useLocation().search;
  const queryParams = new URLSearchParams(queryString);
  const query = queryParams.get("q");

  // const url =
  //   "https://run.mocky.io/v3/58770279-0738-4578-a1cf-c56a193fce98?q=" + query;
  // const { error, isPending, data } = useFetch(url);

  const { emails } = useSelector((store) => store.email);
  console.log(emails);
  const filteredEmail = emails.filter((e) => {
    return e.subject.includes(query) || e.body.includes(query);
  });
  if (filteredEmail.length === 0) {
    return <h4 className="loading-message">No result for search term.</h4>;
  }
  return (
    <div>
      <h2 className="page-title">emails including "{query}"</h2>

      <div className="preview-email-container">
        {filteredEmail.map((d) => (
          <div key={d.id} className="email">
            <Link to={`/emails/${d.id}`}>
              <div className="preview-email">
                <span className="preview-email-subject">{d.subject}</span>
                <span className="preview-email-body"> - {d.body}</span>
              </div>
            </Link>
          </div>
        ))}
      </div>
    </div>
  );
}
