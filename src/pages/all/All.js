import { useDispatch } from "react-redux";
import { useEffect } from "react";
import { setFilter, filterEmails } from "../../features/email/emailSlice";

//css
import "./All.css";

//components and pages
import EmailList from "../../components/EmailList";

export default function All() {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(setFilter("all"));
    dispatch(filterEmails("all"));
  }, [dispatch]);

  return (
    <div>
      <h2>All</h2>
      <div className="main-container">
        <main>
          <EmailList />
        </main>
      </div>
    </div>
  );
}
