import { useDispatch } from "react-redux";
import { useEffect } from "react";
import { setFilter, filterEmails } from "../../features/email/emailSlice";

//css
import "./Draft.css";

//components and pages
import EmailList from "../../components/EmailList";

export default function Draft() {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(setFilter("draft"));
    dispatch(filterEmails("draft"));
  }, [dispatch]);

  return (
    <div>
      <h2>Draft</h2>
      <div className="main-container">
        <main>
          <EmailList />
        </main>
      </div>
    </div>
  );
}
