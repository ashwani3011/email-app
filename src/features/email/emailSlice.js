import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import data from "../../data";

const url = "https://run.mocky.io/v3/58770279-0738-4578-a1cf-c56a193fce98";

const initialState = {
  emails: [],
  filter: "inbox",
  isLoading: true,
};

export const getAllEmails = createAsyncThunk("email/getAllEmails", () => {
  return fetch(url)
    .then((resp) => resp.json())
    .catch((err) => console.log(err));
});

const emailSlice = createSlice({
  name: "email",
  initialState,
  reducers: {
    filterEmails: (state, action) => {
      state.emails = data;
      state.emails = state.emails.filter((e) => {
        if (action.payload === "all") {
          return true;
        }
        return e.tag === action.payload;
      });
    },
    setFilter: (state, action) => {
      state.filter = action.payload;
    },
  },

  extraReducers: {
    [getAllEmails.pending]: (state) => {
      state.isLoading = true;
    },
    [getAllEmails.fulfilled]: (state, action) => {
      state.isLoading = false;
      state.emails = action.payload;
    },
    [getAllEmails.rejected]: (state) => {
      state.isLoading = false;
    },
  },
});

// console.log(emailSlice);

export const { filterEmails, setFilter } = emailSlice.actions;

export default emailSlice.reducer;
